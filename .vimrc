set nocompatible              " be iMproved, required
filetype off                  " required

call plug#begin('~/.config/nvim/plugged')
"Plugin 'scrooloose/syntastic'
Plug 'w0rp/ale'
Plug 'arcticicestudio/nord-vim'
Plug 'neoclide/coc.nvim', {'branch': 'release' }
call plug#end()            " required

filetype plugin indent on
set t_Co=256
"colors
colorscheme nord
highlight Comment ctermbg=white
set hlsearch
set number

" real tabs, with proper autoindent
set noexpandtab
set tabstop=4
set shiftwidth=4

" YCM Config
let g:ycm_python_binary_path = '/bin/python3'
let g:ycm_key_list_stop_completion = ['<C-y>', '<CR>']
let g:ycm_clangd_binary_path = "clangd"
" ALE Config
let g:ale_c_gcc_options = '-std=c18 -Wall'
