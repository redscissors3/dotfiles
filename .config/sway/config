include /etc/sway/config.d/*
# This defines which key super maps to on your keyboard.
# Alt key is Mod1, and Windows key is Mod4
set $mod Mod4
set $alt Mod1
set $shadowrealm scratchpad

# monitor vars
set $prime_out DP-1
set $right_up_out HDMI-A-1
set $right_out DP-2

set $terminal 'alacritty msg create-window || alacritty'
set $terminal_run 'alacritty msg create-window -e'
set $browser flatpak run org.mozilla.firefox

# These are the labels which define each i3 workspace.
set $ws1  "1"
set $ws2  "2"
set $ws3  "3"
set $ws4  "4"
set $ws5  "5"
set $ws6  "6"
set $ws7  "7"
set $ws8  "8"
set $ws9  "9"
set $ws10 "10"
set $ws11 "11"
set $ws12 "12"
set $ws13 "13"
set $ws14 "14"
set $ws15 "15"
set $ws16 "16"
set $ws17 "17"
set $ws18 "18"
set $ws19 "19"

# Configure outputs
output $prime_out {
        background ~/Pictures/Wallpapers/aurora-borealis-sky-4k-bm-2560x1440.jpg fill
        mode 3440x1440
        position 60 720
}

output $right_out {
        background ~/Pictures/Wallpapers/aurora-borealis-sky-4k-bm-2560x1440.jpg fill
        mode 1920x1080
        position 3500 1080
}

output $right_up_out {
         background ~/Pictures/Wallpapers/aurora-borealis-sky-4k-bm-2560x1440.jpg fill
         mode 1920x1080
         position 3500 0
}

xwayland enable

# Configure input devices

input type:pointer {
        accel_profile flat
        pointer_accel  0.2
        scroll_factor 1
        dwt false
}

#Configure look and feel
set $gschema = org.gnome.desktop.interface
exec_always {
        gsettings set $gschema gtk-theme    'Nordic'
        gsettings set org.gnome.desktop.wn.preferences theme "Nordic"
        gsettings set $gschema cursor-theme 'breeze-cursor'
        gsettings set $gschema cursor-size   24
        gsettings set $gschema font-name     'Fantasque Sans Mono'
}

seat seat0 xcursor_theme Breeze 24
font 'Fantasque Sans Mono' 12

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec $terminal

# start a web browser
bindsym $mod+Shift+Return exec $browser

# kill focused window
bindsym $mod+Shift+q kill

# kill all in workspace EXCEPT focused window

bindsym $mod+$alt+Shift+q mark lone_survivor; move $shadowrealm;[workspace=__focused__] kill;[con_mark=lone_survivor] $shadowrealm show;floating toggle;unmark lone_survivor

# fuzzel app launcher
bindsym $mod+space exec fuzzel

# change focus
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# move windows in workspaces
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# move workspaces to outputs
bindsym $mod+Ctrl+Shift+Left move workspace to output left
bindsym $mod+Ctrl+Shift+Down move workspace to output down
bindsym $mod+Ctrl+Shift+Up move workspace to output up
bindsym $mod+Ctrl+Shift+Right move workspace to output right

# move toggle Spotify visibility 
bindsym $mod+s [class="Spotify"] $shadowrealm show

# toggle split orientation
bindsym $mod+BackSpace split toggle

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle
bindsym $mod+$alt+f fullscreen toggle global

# toggle tiling / floating
bindsym $mod+Shift+f floating toggle
bindsym $mod+Ctrl+m move to scratchpad
bindsym $mod+Ctrl+a scratchpad show

# toggle tabbed mode
bindsym $mod+t layout toggle splith splitv
bindsym $mod+Shift+t layout toggle split stacking tabbed

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10
bindsym $mod+Ctrl+1 workspace number $ws11
bindsym $mod+Ctrl+2 workspace number $ws12
bindsym $mod+Ctrl+3 workspace number $ws13
bindsym $mod+Ctrl+4 workspace number $ws14
bindsym $mod+Ctrl+5 workspace number $ws15
bindsym $mod+Ctrl+6 workspace number $ws16
bindsym $mod+Ctrl+7 workspace number $ws17
bindsym $mod+Ctrl+8 workspace number $ws18
bindsym $mod+Ctrl+9 workspace number $ws19


bindsym $mod+mod2+KP_1 workspace number $ws1
bindsym $mod+mod2+KP_2 workspace number $ws2
bindsym $mod+mod2+KP_3 workspace number $ws3
bindsym $mod+mod2+KP_4 workspace number $ws4
bindsym $mod+mod2+KP_5 workspace number $ws5
bindsym $mod+mod2+KP_6 workspace number $ws6
bindsym $mod+mod2+KP_7 workspace number $ws7
bindsym $mod+mod2+KP_8 workspace number $ws8
bindsym $mod+mod2+KP_9 workspace number $ws9
bindsym $mod+mod2+KP_0 workspace number $ws10
bindsym $mod+Ctrl+mod2+KP_1 workspace number $ws11
bindsym $mod+Ctrl+mod2+KP_2 workspace number $ws12
bindsym $mod+Ctrl+mod2+KP_3 workspace number $ws13
bindsym $mod+Ctrl+mod2+KP_4 workspace number $ws14
bindsym $mod+Ctrl+mod2+KP_5 workspace number $ws15
bindsym $mod+Ctrl+mod2+KP_6 workspace number $ws16
bindsym $mod+Ctrl+mod2+KP_7 workspace number $ws17
bindsym $mod+Ctrl+mod2+KP_8 workspace number $ws18
bindsym $mod+Ctrl+mod2+KP_9 workspace number $ws19

# cycle across workspaces
bindsym $mod+Tab workspace next
bindsym $mod+Shift+Tab workspace prev

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10
bindsym $mod+Shift+Ctrl+1 move container to workspace number $ws11
bindsym $mod+Shift+Ctrl+2 move container to workspace number $ws12
bindsym $mod+Shift+Ctrl+3 move container to workspace number $ws13
bindsym $mod+Shift+Ctrl+4 move container to workspace number $ws14
bindsym $mod+Shift+Ctrl+5 move container to workspace number $ws15
bindsym $mod+Shift+Ctrl+6 move container to workspace number $ws16
bindsym $mod+Shift+Ctrl+7 move container to workspace number $ws17
bindsym $mod+Shift+Ctrl+8 move container to workspace number $ws18
bindsym $mod+Shift+Ctrl+9 move container to workspace number $ws19

# move focused container to workspace, move to workspace
bindsym $mod+$alt+1 move container to workspace number $ws1; workspace number $ws1  
bindsym $mod+$alt+2 move container to workspace number $ws2; workspace number $ws2
bindsym $mod+$alt+3 move container to workspace number $ws3; workspace number $ws3
bindsym $mod+$alt+4 move container to workspace number $ws4; workspace number $ws4
bindsym $mod+$alt+5 move container to workspace number $ws5; workspace number $ws5
bindsym $mod+$alt+6 move container to workspace number $ws6; workspace number $ws6
bindsym $mod+$alt+7 move container to workspace number $ws7; workspace number $ws7
bindsym $mod+$alt+8 move container to workspace number $ws8; workspace number $ws8
bindsym $mod+$alt+9 move container to workspace number $ws9; workspace number $ws9
bindsym $mod+$alt+0 move container to workspace number $ws10; workspace number $ws10
bindsym $mod+$alt+Ctrl+1 move container to workspace number $ws11; workspace number $ws11
bindsym $mod+$alt+Ctrl+2 move container to workspace number $ws12; workspace number $ws12
bindsym $mod+$alt+Ctrl+3 move container to workspace number $ws13; workspace number $ws13
bindsym $mod+$alt+Ctrl+4 move container to workspace number $ws14; workspace number $ws14
bindsym $mod+$alt+Ctrl+5 move container to workspace number $ws15; workspace number $ws15
bindsym $mod+$alt+Ctrl+6 move container to workspace number $ws16; workspace number $ws16
bindsym $mod+$alt+Ctrl+7 move container to workspace number $ws17; workspace number $ws17
bindsym $mod+$alt+Ctrl+8 move container to workspace number $ws18; workspace number $ws18
bindsym $mod+$alt+Ctrl+9 move container to workspace number $ws19; workspace number $ws19

# media keys
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym XF86AudioPlay exec playerctl play-pause
bindsym KP_Begin 	  exec playerctl play-pause
bindsym XF86AudioNext exec playerctl next
bindsym KP_Right 	  exec playerctl next
bindsym XF86AudioPrev exec playerctl previous
bindsym KP_Left 	  exec playerctl previous


# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# lock the screen
bindsym $mod+Escape exec /usr/bin/wlogout --protocol layer-shell -b 4 -L 900 -R 900 -T 550 -B 550


# i3-snapshot for load/save current layout
bindsym $mod+comma  exec /usr/bin/i3-snapshot -o > /tmp/i3-snapshot 
bindsym $mod+period exec /usr/bin/i3-snapshot -c < /tmp/i3-snapshot

# resize window (you can also use the mouse for that)
mode "Resize Mode" {
        # These bindings trigger as soon as you enter the resize mode
        bindsym Left resize shrink width 6 px or 6 ppt
        bindsym Down resize grow height 6 px or 6 ppt
        bindsym Up resize shrink height 6 px or 6 ppt
        bindsym Right resize grow width 6 px or 6 ppt

        bindsym Shift+Left resize shrink width 12 px or 12 ppt
        bindsym Shift+Down resize grow height 12 px or 12 ppt
        bindsym Shift+Up resize shrink height 12 px or 12 ppt
        bindsym Shift+Right resize grow width 12 px or 12 ppt

        bindsym h resize shrink width 6 px or 6 ppt
        bindsym k resize grow height 6 px or 6 ppt
        bindsym j resize shrink height 6 px or 6 ppt
        bindsym l resize grow width 6 px or 6 ppt

        bindsym Shift+h resize shrink width 12 px or 12 ppt
        bindsym Shift+k resize grow height 12 px or 12 ppt
        bindsym Shift+j resize shrink height 12 px or 12 ppt
        bindsym Shift+l resize grow width 12 px or 12 ppt

        # change gaps interactively
        bindsym minus gaps inner current minus 6
        bindsym plus gaps inner current plus 6
        bindsym Shift+minus gaps inner current minus 12
        bindsym Shift+plus gaps inner current plus 12

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}
bindsym $mod+r mode "Resize Mode"

default_border pixel 2
default_floating_border pixel 2

# Specify the distance between windows in pixels. (i3-gaps)
gaps inner 15
gaps outer 5

# Only enable gaps on a workspace when there is at least one container
smart_gaps inverse_outer

smart_borders off
# Create variables for i3's look.

set $clear #ffffff00
set $focused.color.border               "#b48ead"
set $focused.color.background           "#4C566A"
set $focused.color.text                 "#ECEFF4"
set $focused.color.indicator            "#A3BE8C"
set $focused.color.child_border         "#B48EAD"

set $urgent.color.border                "#BF616A"
set $urgent.color.background            "#BF616A"
set $urgent.color.text                  "#ECEFF4"
set $urgent.color.indicator             "#A3BE8C"
set $urgent.color.child_border          "#BF616A"

set $unfocused.color.border             $clear
set $unfocused.color.background         "#2E3440"
set $unfocused.color.text               "#D8DEE9"
set $unfocused.color.indicator          $clear
set $unfocused.color.child_border       $clear

# Window Border color
# class                         border                          background                    	        text                    indicator                       child_border
client.focused                  $focused.color.border    	$focused.color.background 		$focused.color.text     $focused.color.indicator        $focused.color.child_border
client.urgent                   $urgent.color.border      	$urgent.color.background                $urgent.color.text      $urgent.color.indicator   	$urgent.color.child_border
client.unfocused        	$unfocused.color.border         $unfocused.color.background             $unfocused.color.text   $unfocused.color.indicator      $unfocused.color.child_border
client.focused_inactive         $unfocused.color.border         $unfocused.color.background             $unfocused.color.text   $unfocused.color.indicator      $unfocused.color.child_border

# Enable popup during fullscreen
popup_during_fullscreen smart

# window focus follows your mouse movements as the mouse crosses window borders
focus_follows_mouse no
# mouse warp moves mouse pointer to other monitor when window switching
mouse_warping container

# press workspace switch again to go back to previous workspace
workspace_auto_back_and_forth yes

# send spotify to shadowrealm.  Workaround cause spotify sucks: https://wiki.archlinux.org/index.php/i3#Default_workspace_for_Spotify
for_window [class="Spotify"] move $shadowrealm

# workspace setup
workspace $ws1 output $prime_out

workspace $ws2 output $right_up_out

workspace $ws4 output $right_out


assign [app_id=".*musics"] $ws2

workspace $ws3 output $prime_out
assign [app_id="firefox"] $ws3

workspace $ws4 output $right_out
assign [class="discord"] $ws4

workspace $ws5 output $prime_out
assign [class="steam"] $ws5

bar {
swaybar_command waybar
}

# terminal daemon.  all autostarted terminals must be chained after this one.  Even though it looks sloppy :(
exec --no-startup-id $terminal

# startup apps
exec --no-startup-id "sh -c 'sleep 2; alacritty msg create-window --class lyrics_musics -e lyrics'"
exec --no-startup-id "sh -c 'sleep 2; alacritty msg create-window --class visualizer_musics -e cava'"
exec --no-startup-id flatpak run com.valvesoftware.Steam
exec --no-startup-id $browser
exec --no-startup-id flatpak run com.spotify.Client
exec --no-startup-id flatpak run com.discordapp.Discord
exec --no-startup-id blueman-tray
exec --no-startup-id insync start
exec --no-startup-id lxpolkit
